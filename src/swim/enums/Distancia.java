package swim.enums;

public enum Distancia {

	_50,
	_100,
	_200,
	_400,
	_800,
	_1500,
	
	_4X50,
	_4X100,
	_4X200,
	
	_8X50

}
