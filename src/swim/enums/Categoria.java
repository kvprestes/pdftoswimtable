package swim.enums;

public enum Categoria {
	ABSOLUTO,
	PRE_MIRIM,
	MIRIM,
        MIRIM_1,
        MIRIM_2,
	PETIZ,
        PETIZ_1,
        PETIZ_2,
	INFANTIL,
		INFANTIL_1,
		INFANTIL_2,
	JUVENIL, 
		JUVENIL_1,
		JUVENIL_2,
	JUNIOR,
		JUNIOR_1,
		JUNIOR_2,
	SENIOR,
        JUVENIL_A_SENIOR
}
