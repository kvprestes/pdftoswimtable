package swim.enums;

public enum Estilo {

	BORBOLETA,
	COSTAS,
	PEITO,
	LIVRE,
	MEDLEY
}
