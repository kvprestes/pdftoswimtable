package swim.enums;

public enum Naipe {
	FEM,
	FEMININO,
	MASC,
	MASCULINO,
    MISTO
}
