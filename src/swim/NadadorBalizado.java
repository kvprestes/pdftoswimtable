package swim;

public class NadadorBalizado {

	private int raia;
	private String nome;
	private long codCBDA;
	private int anoNascimento;
	private String clube;
	private String tempo;
	
	public int getRaia() {
		return raia;
	}
	public void setRaia(int raia) {
		this.raia = raia;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public long getCodCBDA() {
		return codCBDA;
	}
	public void setCodCBDA(long codCBDA) {
		this.codCBDA = codCBDA;
	}
	public int getAnoNascimento() {
		return anoNascimento;
	}
	public void setAnoNascimento(int anoNascimento) {
		this.anoNascimento = anoNascimento;
	}
	public String getClube() {
		return clube;
	}
	public void setClube(String clube) {
		this.clube = clube;
	}
	public String getTempo() {
		return tempo;
	}
	public void setTempo(String tempo) {
		this.tempo = tempo;
	}
	public String getPrimeiroNome(){
		return this.nome.substring(0, this.nome.indexOf(" "));
	}
    public String getUltimoNome(){
    	return this.nome.substring(this.nome.lastIndexOf(" ")+1);
    }
	
}
