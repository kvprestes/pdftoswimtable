package swim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import swim.enums.Categoria;
import swim.enums.Distancia;
import swim.enums.Estilo;
import swim.enums.Naipe;

public class Prova {

    private int numero;
    private Distancia distancia;
    private Estilo estilo;
    private Naipe naipe;
    private Categoria categoria;

    //usado na leitura de balizamento
    private ArrayList<Serie> series;

    public Prova() {
        this.series = new ArrayList<Serie>();
    }

    public Prova(String prova) {
        this.lerBalizmentoProva(prova);
    }

    public void lerBalizmentoProva(String prova) {
        String s[] = prova.split("SÉRIE");
        for (int i = 1; i < s.length; i++) {
            //System.out.println(series.get(i));
            //System.out.println("_______________________________");
            this.series.add(lerSerie(s[i]));
        }
    }

    private Serie lerSerie(String serie) {
        Serie s = new Serie(serie);
        return s;
    }

    public Distancia getDistancia() {
        return distancia;
    }

    public void setDistancia(Distancia distancia) {
        this.distancia = distancia;
    }

    public Estilo getEstilo() {
        return estilo;
    }

    public void setEstilo(Estilo estilo) {
        this.estilo = estilo;
    }

    public ArrayList<Serie> getSeries() {
        return series;
    }

    public void setSeries(ArrayList<Serie> series) {
        this.series = series;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Naipe getNaipe() {
        return naipe;
    }

    public void setNaipe(Naipe n) {
        this.naipe = n;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public String toString() {
        return this.getDistancia().toString().replace("_", "") + " " + this.getEstilo().toString().charAt(0);
    }
    
    public Set<String> getClubes(){
    	Set<String> ret = new HashSet<String>();
        for (int i = 0; i < series.size(); i++) {
            ret.addAll(series.get(i).getClubes());
        }
        return ret;
    }

}
