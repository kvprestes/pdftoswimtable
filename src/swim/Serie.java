package swim;

import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Uma serie tem ate 8 @see NadadorBalizado
 *
 * @author kassius
 *
 */
public class Serie {

    NadadorBalizado[] nadadores;

    public Serie(String serie) {
        nadadores = new NadadorBalizado[10];

        //TODO expressao regular para obter corretamente revezamentos
        
        //String tempSlash = "_SLASH_"; //nao tava funcionando com a barra '/'
        /*
		 * formato da linha
		 * raia	\\d
		 * nome [\\w\\s]*
		 * codigo CBDA \\d*
		 * ano de nascimento \\d\\d\\d\\d
		 * clube [\\w(_SLASH_)]*
		 * tempo \\d\\d:\\d\\d.\\d\\d
         */
        Pattern p = Pattern.compile("(?<raia>\\d)\\s"
                + "(?<nome>[a-zA-Z(/\\s)ÁÃÂÉÊẼÍĨÎÓÕÔÚŨÛ.Ç'\"-]*)\\s"
                + "(?<codCBDA>V?\\d*)\\s"
                + "(?<ano>\\d\\d\\d\\d[A-Z]?)\\s"
                + "(?<clube>[a-zA-Z(/\\s)]*)\\s?"
                + "(?<tempo>\\d\\d:\\d\\d.\\d\\d)?");
        //serie = serie.replaceAll("/", tempSlash);
        serie = serie.trim().replaceAll("( )+", " ");//remove espaços em branco redundantes
        Matcher matcher = p.matcher(serie);
        NadadorBalizado n;
        while (matcher.find()) {
/*
            System.out.println("===================================================");
            System.out.println(matcher.group());
            System.out.println(matcher.group("raia"));
            System.out.println(matcher.group("nome"));
            System.out.println(matcher.group("codCBDA"));
            System.out.println(matcher.group("ano"));
            System.out.println(matcher.group("clube"));
            System.out.println(matcher.group("tempo"));
            System.out.println("===================================================");
*/
            n = new NadadorBalizado();
            n.setRaia(Integer.parseInt(matcher.group("raia")));
            n.setNome(matcher.group("nome").trim());
            String codCBDA = matcher.group("codCBDA");
            if (codCBDA.startsWith("V")) {
                n.setCodCBDA(Long.parseLong(codCBDA.substring(1)));
            }
            n.setAnoNascimento(Integer.parseInt(matcher.group("ano")));
            n.setClube(matcher.group("clube").trim());
            n.setTempo(matcher.group("tempo"));
            if (n.getRaia() > 9 || n.getRaia() < 0) {
                System.out.println("erro no número da raia");
            }
            // quando as raias eram de 0 a 8, tinha que diminuir 1 do indice
            // agora uso direto o numero da raia porque podem ir de 0 a 9
            nadadores[n.getRaia()] = n;
        }
    }

    public NadadorBalizado[] getNadadores() {
        return nadadores;
    }

    public void setNadadores(NadadorBalizado[] nadadores) {
        this.nadadores = nadadores;
    }

    public Set<String> getClubes() {
        Set<String> ret = new HashSet<>();
        for (NadadorBalizado n : nadadores) {
            if (n != null) {
                ret.add(n.getClube());
            }
        }
        return ret;
    }

}
