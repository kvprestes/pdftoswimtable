package swim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import readPdf.result.ReadResultLine;

public class ProvaFinalizada extends Prova{

    private ArrayList<QuedaNaAgua> resultadoFinal;
    
    public ProvaFinalizada() {
    	super();
        this.resultadoFinal = new ArrayList<QuedaNaAgua>();
    }
    
    public void lerResultadoProva(String prova) {
        //antes era somente º|°, adicionei as quebras de linha \r\n, e RC no bra inf 2023
        //porque tem um clube que 1° de julho que quebra tudo, e os recordes do campeonato também
        // \r\n = windows, \n = linux
        String quedaNaAguaSeparator = "º\r\n|°\r\n|° RC|º RC|º\n|°\n";
        String s[] = prova.split(quedaNaAguaSeparator);
        for (int i = 0; i < s.length-1; i++) {
            //System.out.println(s[i]);
            //System.out.println("_______________________________");
        	QuedaNaAgua q = lerResultadoFinal(s[i]);
        	if (q!=null){
        		this.resultadoFinal.add(q);
        	}
        	else {
        		System.out.println("Erro ao ler prova:");
        		System.out.println(prova);
        		System.out.println("__________________");
        		break;
        	}
        }
    }
    
    private QuedaNaAgua lerResultadoFinal(String resultado) {
    	QuedaNaAgua r = ReadResultLine.parseLine(resultado);
        return r;
    }
    
	public ArrayList<QuedaNaAgua> getResultadoFinal() {
		return resultadoFinal;
	}
	
	public Set<String> getClubes(){
    	Set<String> ret = new HashSet<String>();
        for (int i = 0; i < resultadoFinal.size(); i++) {
            ret.add(resultadoFinal.get(i).getClube());
        }
        return ret;
    }
}
