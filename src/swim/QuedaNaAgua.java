package swim;

/**
 *
 * @author kassius
 */
public class QuedaNaAgua {
    
    int colocacao;
    int serie;
    int raia;
    String nome;
    String cod_cbda;
    int ano_nascimento;
    String patrocinio;
    String clube;
    String tempo_balizamento;
    String tempo_final;
    String IT;
    String pontos;
    
    public QuedaNaAgua(int colocacao, int serie, int raia, String nome, 
            String cod_cbda, int ano_nascimento, String patrocinio, 
            String clube, String tempo_balizamento, String tempo_final, 
            String IT, String pontos) {
        this.colocacao = colocacao;
        this.serie = serie;
        this.raia = raia;
        this.nome = nome;
        this.cod_cbda = cod_cbda;
        this.ano_nascimento = ano_nascimento;
        this.patrocinio = patrocinio;
        this.clube = clube;
        this.tempo_balizamento = tempo_balizamento;
        this.tempo_final = tempo_final;
        this.IT = IT;
        this.pontos = pontos;
    }
    
    public String toString(){
        return nome + " " + tempo_final;
    }

    public int getColocacao() {
        return colocacao;
    }

    public int getSerie() {
        return serie;
    }

    public int getRaia() {
        return raia;
    }

    public String getNome() {
        return nome;
    }
    
    public String getNomeAbreviado() {
        return nome.substring(0, this.nome.indexOf(" ")) + " " + nome.substring(this.nome.lastIndexOf(" ")+1);
    }

    public String getCod_cbda() {
        return cod_cbda;
    }

    public int getAnoNascimento() {
        return ano_nascimento;
    }

    public String getPatrocinio() {
        return patrocinio;
    }

    public String getClube() {
        return clube;
    }

    public String getTempo_balizamento() {
        return tempo_balizamento;
    }

    public String getTempoFinal() {
        return tempo_final;
    }

    public String getIT() {
        return IT;
    }

    public String getPontos() {
        return pontos;
    }
    
    public void setNome(String n){
        nome = n;
    }
}