package swim;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ResultadoNadador extends NadadorBalizado{

	/**
     * cada regex captura uma parte do texto de um arquivo de resultado
     */
    private static String erColocacao = "(?<colocacao>(\\d+º|N/C|DQL|nullº|OBS|CVD))";
    private static String erSerie = "((?<serie>\\d\\d?)\\s)?";
    private static String erRaia = "((?<raia>\\d\\d?)\\s)?";
    /*existem codigos com 2-6 digitos e uma letra opcional no inicio ou final*/
    private static String erCodCBDA = "(?<codCBDA>([\\dA-Z])?([\\dA-Z])?\\d?\\d?\\d\\d[A-Z]?)\\s";
    private static String erNome = "(?<nome>[a-zA-Z(/\\s)ÁÃÂÉÊẼÍĨÎÓÕÔÚŨÛÇ'\"-]+)";
    private static String erClube = "(?<clube>[a-zA-Z0-9ÁÃÂÉÊẼÍĨÎÓÕÔÚŨÛÇ.'(/\\s)-]*)\\s";
    private static String erTempoBalizamento = "(?<tempoBalizamento>\\d\\d:\\d\\d.\\d\\d)\\s";
    private static String erTempoFinal = "(?<tempoFinal>(\\d\\d:\\d\\d.\\d\\d|-))";
    private static String erPontos = "(\\s(?<pontos>\\d*,\\d\\d?))?"; //sem espaço entre pontos/IT e nome
    
    /**
     * Cria matcher para linhas que terminam com a colocacao
     * @param input
     * @return 
     */
    private static Matcher createAlternativeLineMatcher(String input){
        Pattern p = Pattern.compile(
                  erSerie
                + erRaia
                + erNome + "\\s"
                + erCodCBDA
                + "(?<ano>\\d\\d\\d\\d[A-Z]?)\\s"
                //+ "(?<patrocinio>[a-zA-Z(/\\s)]*)?\\s?"
                + erClube
                + erTempoBalizamento
                + erTempoFinal
                + "(\\s(?<IT>\\d*))?"
                + erPontos
                + erColocacao )
                ;
        Matcher m = p.matcher(input);
        return m;
    }
    
	private String tempoFinal;
	private int colocacao;
	
	public String getTempoFinal() {
		return tempoFinal;
	}
	public void setTempoFinal(String tempoFinal) {
		this.tempoFinal = tempoFinal;
	}
	public int getColocacao() {
		return colocacao;
	}
	public void setColocacao(int colocacao) {
		this.colocacao = colocacao;
	}
}
