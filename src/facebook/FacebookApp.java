/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package facebook;

import java.awt.Desktop;
import java.awt.Dimension;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
//import javafx.scene.web.WebEngine;
//import javafx.scene.web.WebView;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.browser.LocationEvent;
import org.eclipse.swt.browser.LocationListener;
import org.eclipse.swt.browser.TitleEvent;
import org.eclipse.swt.browser.TitleListener;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;

/**
 *
 * @author kassius
 */
public class FacebookApp {

    static String facebookCode;
    
    public static void main(String args[]) throws IOException {
        //String code = "AQC2UREpTFunUxX1Cfnq4UB_t2L5FAg4sQCCPhi6baQb3a47py8K3dotM7ssSd3_HpGs8oumKANE1E8XMS84_t0fB5n29vwkyINMICS0rutMlVstpeKoGO0qR6I9QYZHt_f1l-lAbNCjWjWL4YILUHo7x7BPFVk9PsTNgATeg954nBRa0mqJu74soYpGXHeQG2WQJj0kJpRFFOnpXDtJtdo7oWtxgjp3DYkOLkeZP5G4gatlmwn-TqDPflftdQqrcyGQPCahPf_o7gXnFLHfhenlVPoAY5EUOcrD6WRJexZk1C46JtA3j2lsKOQ4De1Tom6FX3lG5HB4fin8qw4JHEGp#_=_";

        Login l = new Login();
        String firstUrl = l.getLoginRedirectURL();

        //openWebpage(new URL(firstUrl));
        openWebPageSwing(firstUrl);

        //l.obterUsuarioFacebook(code);
    }

    public static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void openWebpage(URL url) {
        try {
            openWebpage(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void openWebPageSwing(String url) {
        /*
        JEditorPane jep = new JEditorPane();
        jep.setEditable(false);

        try {
            jep.setPage(url);
        } catch (IOException e) {
            jep.setContentType("text/html");
            jep.setText("<html>Could not load</html>");
        }

        JScrollPane scrollPane = new JScrollPane(jep);
        JFrame f = new JFrame("Test HTML");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.getContentPane().add(scrollPane);
        f.setPreferredSize(new Dimension(800, 600));
        f.setVisible(true);
         */
    	/*
        WebView browser = new WebView();
        WebEngine webEngine = browser.getEngine();
        webEngine.load(url);
         */
        Browser browser;
        Display display = new Display();
        final Shell shell = new Shell(display);
        shell.setText("Browser Example");
        shell.setSize(800, 600);
        try {
            browser = new Browser(shell, SWT.NONE);
            shell.open();
            browser.setUrl(url);
            browser.setBounds(0, 0, 800, 600);
            browser.addTitleListener(new TitleListener() {
                public void changed(TitleEvent event) {
                    shell.setText(event.title);
                    System.out.println("changed title");
                }
            });
            browser.addLocationListener(new LocationListener(){
                @Override
                public void changing(LocationEvent le) {
                    //System.out.println("changing location " + le.location);
                    int index = le.location.indexOf("https://localhost:8080/PdfToSwimTable/");
                    if (index >= 0)
                    {
                        facebookCode = le.location.substring(index + "https://localhost:8080/PdfToSwimTable/?code=".length());
                        System.out.println("got facebook code");
                        System.out.println(facebookCode);
                        //changing location https://localhost:8080/PdfToSwimTable/?code=AQAv_VAgsE8PsuU-VEC3ejVQRFmXFEQ44fn-cYtTvOKZJCSxCk_Werz2Z9cGdTGK9oENek3-WhpbZ7PiYV8lVpxS5ERyqmSMc6MuJANuE7Hd13oBZQxxpjmLDP-BW2M2IbJLvVYdOTgsdjxFyMCC5l1hVl7-UucHBpvMxfSMVn5tDN34e-XYPcD_BoqQN_mSpmBsCiNfplc9FBjIovH_SXBXCL9YPwuYq2uLRgJs_LdIQm_x-ejeX40T4Jwsbdu8OMx6uurInkjsvxhYhupMGpKMOcfRVjMbKK6rqbPsm6gIE_wyasY8BpfNwS57pbL3GqwJzNpqQ9QWGgYSvRkQTlDi#_=_
                    }
                }

                @Override
                public void changed(LocationEvent le) {
                    //System.out.println("changed location" + le.location);
                }
            
            });
            while (!shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
            display.dispose();
        } catch (SWTError e) {
            MessageBox messageBox = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
            messageBox.setMessage("Browser cannot be initialized.");
            messageBox.setText("Exit");
            messageBox.open();
            System.exit(-1);
        }
    }

}
