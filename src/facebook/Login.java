package facebook;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import org.json.JSONObject;


// https://www.concretesolutions.com.br/2014/05/22/como-fazer-login-com-facebook-usando-java-client-oauth-2-0/

public class Login {

    private static final String client_secret = "431ac3734224656829f7428808a0d5e9";
    private static final String client_id = "2057583774469269";
    private static final String redirect_uri = "https://localhost:8080/PdfToSwimTable/";

    public void obterUsuarioFacebook(String code)
            throws MalformedURLException, IOException {

        String retorno = readURL(new URL(this.getAuthURL(code)));

        String accessToken = null;
        @SuppressWarnings("unused")
        Integer expires = null;
        String[] pairs = retorno.split("&");
        for (String pair : pairs) {
            String[] kv = pair.split("=");
            if (kv.length != 3) {
                System.out.println(retorno);
                throw new RuntimeException("Resposta auth inesperada.");
            } else {
                if (kv[0].equals("access_token")) {
                    accessToken = kv[1];
                }
                if (kv[0].equals("expires")) {
                    expires = Integer.valueOf(kv[1]);
                }
                // other reponse received is "token_type" = "bearer"
            }
        }

        JSONObject resp = new JSONObject(readURL(new URL(
                "https://graph.facebook.com/me?access_token=" + accessToken)));
        
        System.out.print(resp);
        //UsuarioFacebook usuarioFacebook = new UsuarioFacebook(resp);
        //System.out.println(usuarioFacebook.toString());

    }

    private String readURL(URL url) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = url.openStream();
        int r;
        while ((r = is.read()) != -1) {
            baos.write(r);
        }
        return new String(baos.toByteArray());
    }

    public String getLoginRedirectURL() {
        return "https://graph.facebook.com/oauth/authorize?client_id="
                + client_id + "&display=page&redirect_uri=" + redirect_uri
                + "&scope=email";
    }

    public String getAuthURL(String authCode) {
        return "https://graph.facebook.com/oauth/access_token?client_id="
                + client_id
                + "&client_secret=" + client_secret
                + "&redirect_uri=" + redirect_uri
                + "&code=" + authCode
                ;
    }

}
