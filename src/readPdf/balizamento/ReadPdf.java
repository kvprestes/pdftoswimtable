package readPdf.balizamento;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import pesquisa.BuscaPorClube;
import readPdf.IReadPdf;
import swim.Prova;
import swim.enums.Categoria;
import swim.enums.Distancia;
import swim.enums.Estilo;
import swim.enums.Naipe;

public class ReadPdf implements IReadPdf{
	
	private String clubeTeste = "CAIXEIROS";
	//estadual de inverno 2023
	private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/89dc2de24291020c7bc31b3cdf28f473.pdf";
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/356bc86a9e26f86264a7d1ed6be81df0.pdf";
	//private String pdfUrl = "file:/home/kassius/workspace/PdfToSwimTable/estadual_inverno_mirim_petiz.pdf";
    //private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/36045/10360450000000F000000000000000000001351.pdf";
    //private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/boletim/o_1ftq4htl31riglbd1mvr1qdh3g2b.pdf";
    //private String pdfUrl = "https://sistema.cbdaweb.org.br/cbdaweb/_uploads/piscina/34317/103431700000000000000000000000000001128.pdf?x=1573828871";
    //private String pdfUrl = "http://www.fgda.com.br/site/wp-content/uploads/2013/04/BALIZAMENTO-TORNEIO-SUL-BRASILEIRO-DE-CLUBES-MIRIM-E-PETIZ-DE-NATA%C3%87%C3%83O-.pdf";
    //private String pdfUrl = "http://www.fgda.com.br/site/wp-content/uploads/2013/04/BALIZAMENTO-CAMPEONATO-ESTADUAL-DE-NATA%C3%87%C3%83O-DE-VER%C3%83O-MIRIM-A-SENIOR-2016.pdf";
    //private String pdfUrl = "http://www.fgda.com.br/site/wp-content/uploads/2013/04/BALIZAMENTO-TORNEIO-DE-ABERTURA-DE-TEMPORADA-2017.pdf";

    /**
     * Expressao regular que identifica uma prova
     */
    private static final String regexpProva = "(?<distancia>50|100|200|400|800|1500|4X50|4X100|4X200|R8X50)L?M?\\s?"
            + "(?<estilo>BORBOLETA|COSTAS|PEITO|LIVRE|L|MEDLEY)\\s?"
            + "(?<naipe>FEM|MASC|MISTO|MI)(.)*"
            //+ "(?<numProva>PROVA(\\d)*)"
            + "(?<categoria>ABSOLUTO|MIRIM|MIRIM 1|MIRIM 2|PETIZ|PETIZ 1|PETIZ 2|INFANTIL|JUVENIL|Juvenil|JUNIOR|Júnior|SENIOR|Sênior"
            + "|Juvenil a Sênior)?";

    /**
     * cabecalhos do pdf, que devem excluidos
     */
    private ArrayList<String> cabecalhos;
    /**
     * rodapes do pdf, tambem devem ser excluidos
     */
    private ArrayList<String> rodape;

    /**
     * lista de Provas do Pdf, contem todas as informaçoes da competicao
     */
    private ArrayList<Prova> provas;
    /**
     * lista de Clubes presentes nas provas, usados para filtragem
     */
    private HashSet<String> clubes;
    /**
     * prova em que cada etapa inicia, obviamente a etapa 1 comeca na prova 1
     */
    private ArrayList<Integer> inicioEtapas;
    
    /**
     * nome da competicao
     */
    private String nomeCompeticao;

    /**
     * Construtor
     */
    public ReadPdf() {
        provas = new ArrayList<Prova>();
        cabecalhos = new ArrayList<String>();
        rodape = new ArrayList<String>();
        clubes = new HashSet<String>();
        inicioEtapas = new ArrayList<>();
    }

    /**
     * obtem cabecalhos do pdf, utilizados para filtragem
     * @param s
     */
    private void obterCabecalhos(String s) {
        String[] p = s.split("\n");
        for (int i = 0; i < p.length - 1; i++) {	//ultima linha faz parte da primeira prova
            cabecalhos.add(p[i]);
        }
    }

    /**
     *
     * @param docSplit
     */
    private void obterRodape(String[] docSplit) {
        for (int i = 0; i < docSplit.length; i++) {
            if (docSplit[i].contains("Versão")) {
                String[] lines = docSplit[i].split("\n");
                for (String l : lines) {
                    if (l.contains("Versão")) {
                        rodape.add(l);
                        //System.out.println(l);
                    }
                }
            }
        }
    }

    private void criaListaDeProvas(String doc) {
        Pattern p = Pattern.compile(regexpProva);
        Matcher matcher = p.matcher(doc.toUpperCase().replaceAll("Ú", "U").replaceAll("Ê", "E")); //Júnior, Sênior
        int i = 0;
        Prova prova;
        while (matcher.find()) {
            i++;
            prova = new Prova();
            prova.setNumero(i);
            prova.setDistancia(Distancia.valueOf("_" + matcher.group("distancia").replace("R8X50", "8X50")));
            String estilo = matcher.group("estilo");
            if (estilo.equals("L")) estilo = "LIVRE";
            prova.setEstilo(Estilo.valueOf(estilo));
            String naipe = matcher.group("naipe");
            if (naipe.equals("MI")) naipe = "MISTO";
            prova.setNaipe(Naipe.valueOf(naipe));
            String categoria = matcher.group("categoria");
            if (categoria != null) prova.setCategoria(Categoria.valueOf(categoria.toUpperCase().replaceAll(" ", "_")));
            //System.out.println(matcher.group());
            this.provas.add(prova);
        }
        //System.out.println("total "+ i);
    }

    public void lerPdf() {
        System.setProperty("http.agent", "Chrome"); //sem isso da erro 403
        InputStream input;
        try {
            input = new URL(this.getPdfUrl()).openStream();

            PDDocument pddDocument = PDDocument.load(input);
            PDFTextStripper textStripper = new PDFTextStripper();
            String doc = textStripper.getText(pddDocument);
            pddDocument.close();

            nomeCompeticao = doc.substring(0, doc.indexOf("\n")).trim();
            
            doc = doc.replaceAll("BORBOL\\. |BORBOL ", "BORBOLETA ");
            
            //System.out.println(doc);

            String[] splitPorProva = doc.split(ProvaDelimiter);

            //obtem cabecalhos e rodapes, exclui do documento, e separa por provas de novo
            this.obterCabecalhos(splitPorProva[0]);
            for (String c : cabecalhos) {
                doc = doc.replace(c, "");
            }
            this.obterRodape(splitPorProva);
            for (String r : rodape) {
                doc = doc.replace(r, "");
            }
            splitPorProva = doc.split(ProvaDelimiter);

            this.criaListaDeProvas(doc);

            for (int i = 1; i < splitPorProva.length; i++) {
                int numProva;
                if (i < splitPorProva.length - 1){
                    numProva = Integer.parseInt(splitPorProva[i+1].substring(0, splitPorProva[i+1].indexOf(" ")));
                }
                else {
                    numProva = provas.get(i - 2).getNumero() + 1;
                }
                if (splitPorProva[i].contains("Etapa")){
                    //nao da pra usar o i, pois algumas provas nao existem - ex. 23 no estadual de verao 2016
                    //vou o primeiro numero que aparece na proxima prova
                    inicioEtapas.add(numProva);
                }
                //System.out.println(splitPorProva[i-1]);
                //System.out.println(provas.size() +" " + i + "" + numProva +"________________________________");
                provas.get(i - 1).lerBalizmentoProva(splitPorProva[i]);
                provas.get(i - 1).setNumero(numProva);
                //p.setDistancia(distancia);
                //p.setEstilo(estilo);
                clubes.addAll(provas.get(i - 1).getClubes());
            }
            //System.out.println("TOTAL LIDO " + provas.size());
        } catch (IOException e) {
            System.err.println("Erro ao obter pdf");
            System.err.println(e.getMessage());
        }
    }

    public ArrayList<Prova> getProvas() {
        return this.provas;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String p) {
        pdfUrl = p;
    }

    public HashSet<String> getClubes() {
        return clubes;
    }
    
    public ArrayList<Integer> getInicioEtapas(){
        return inicioEtapas;
    }
    
    public String getNomeCompeticao(){
        return nomeCompeticao;
    }

    /**
     * main para testar o programa sem a interface grafica
     * @param args 
     */
    public static void main(String args[]) {
        ReadPdf leitorPdf = new ReadPdf();
        if (args.length > 1) {
            leitorPdf.setPdfUrl(args[0]);
        }
        leitorPdf.lerPdf();

        BuscaPorClube b = new BuscaPorClube(leitorPdf.clubeTeste, leitorPdf.nomeCompeticao, leitorPdf.provas);
        b.montaListadeAtletas(true);
        b.writeExcelFile("");
    }
}
