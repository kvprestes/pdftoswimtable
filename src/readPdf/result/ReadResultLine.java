package readPdf.result;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import swim.QuedaNaAgua;

public class ReadResultLine {

	/**
	 * cada regex captura uma parte do texto de um arquivo de resultado
	 */
	private static String erColocacao = "(?<colocacao>(\\d+º|\\d+$|N/C|DQL|nullº|OBS|CVD))";
	private static String erSerie = "((?<serie>\\d\\d?)\\s)?";
	private static String erRaia = "((?<raia>\\d\\d?)\\s)?";
	/* existem codigos com 2-6 digitos e uma letra opcional no inicio ou final */
	private static String erCodCBDA = "(?<codCBDA>([\\dA-Z])?([\\dA-Z])?\\d?\\d?\\d?\\d\\d[A-Z]?)\\s";
	private static String erNome = "(?<nome>[a-zA-Z(/\\s)\\.ÁÃÂÉÊẼÍĨÎÓÕÔÚŨÛÇ'\"-]+)";
	private static String erClube = "(?<clube>[a-zA-Z0-9ÁÃÂÉÊẼÍĨÎÓÕÔÚŨÛÇº.'(/\\s)-]*)\\s?";
	//private static String erTempoBalizamento = "(?<tempoBalizamento>\\d\\d:\\d\\d.\\d\\d)\\s";
	private static String erTempoFinal = "(?<tempoFinal>(\\d\\d:\\d\\d.\\d\\d|-))";
	private static String erPontos = "(?<pontos>\\d+,\\d\\d)*";
	private static String erIT = "(?<IT>0|\\d?\\d?\\d)";

	/**
	 * Cria matcher para linhas que terminam com a colocacao
	 * 
	 * @param input
	 * @return
	 */
	private static Matcher createAlternativeLineMatcher(String input) {
		Pattern p = Pattern.compile(
				erSerie + 
				erRaia + 
				erNome + "\\s" + 
				"(?<ano>\\d\\d\\d\\d[A-Z]?)\\s" + 
				erClube + 
				erTempoFinal + "(\\sE)?" /*EMPATE*/ +  
				erCodCBDA + 
				erIT + 
				erPontos + 
				erColocacao
				);
		Matcher m = p.matcher(input);
		return m;
	}
	
	//lista exaustiva de colocacoes e pontuacoes correspondentes para ler corretamente quando IT esta colado nos pontos
	private static boolean shouldFixIT(Integer pontos, Integer colocacao) {
		return !((colocacao == 1 && (pontos == 18 || pontos == 36)) ||
				(colocacao == 2 && (pontos == 16 || pontos == 32)) ||
				(colocacao == 3 && (pontos == 15 || pontos == 30)) ||
				(colocacao == 4 && (pontos == 14 || pontos == 28)) ||
				(colocacao == 5 && (pontos == 13 || pontos == 26)) ||
				(colocacao == 6 && (pontos == 12 || pontos == 24)) ||
				(colocacao == 7 && (pontos == 11 || pontos == 22)) ||
				(colocacao == 8 && (pontos == 10 || pontos == 20)) ||
				(colocacao == 9 && (pontos == 9 || pontos == 18)) ||
				(colocacao == 10 && (pontos == 8 || pontos == 7 || pontos == 16)) ||
				(colocacao == 11 && (pontos == 7 || pontos == 6 || pontos == 14)) ||
				(colocacao == 12 && (pontos == 6 || pontos == 5 || pontos == 12)) ||
				(colocacao == 13 && (pontos == 5 || pontos == 4 || pontos == 10)) ||
				(colocacao == 14 && (pontos == 4 || pontos == 3 || pontos == 8)) ||
				(colocacao == 15 && (pontos == 3 || pontos == 2 || pontos == 6)) ||
				(colocacao == 16 && (pontos == 2 || pontos == 1 || pontos == 4))
				);
	}

	private static QuedaNaAgua matcherToQuedaNaAgua(Matcher matcher) {
	    // System.out.println(line);
        // System.out.println(matcher.group());
        String colocacao;
        try {
            colocacao = matcher.group("colocacao");
        }
        catch (IllegalStateException e)
        {
            colocacao = "0";
        }
        String pontos = matcher.group("pontos");
        if (pontos == null){
            pontos = "0,00";
        }
        String IT = matcher.group("IT");
        if (shouldFixIT(Integer.parseInt(pontos.substring(0,pontos.indexOf(","))),
                Integer.parseInt(colocacao))) {
            System.out.println("PONTOS/IT ERRADO");
            //System.out.println(line);
            //pega o ultimo digito do IT e coloca nos pontos
            String digit = IT.substring(IT.length()-1);
            String newIT = IT.substring(0,IT.length()-1);
            String newPontos = digit + pontos;
            //somente substitui se corrigir o problema
            if (!(shouldFixIT(Integer.parseInt(newPontos.substring(0,newPontos.indexOf(","))),
                    Integer.parseInt(colocacao)))){
                IT = newIT;
                pontos = newPontos;
            }
        }
        return new QuedaNaAgua(Integer.parseInt(colocacao),
                0, // Integer.parseInt(matcher.group("serie")),
                0, // Integer.parseInt(matcher.group("raia")),
                matcher.group("nome").trim(), 
                matcher.group("codCBDA"), 
                Integer.parseInt(matcher.group("ano")), "",
                matcher.group("clube").trim(),
                "", // matcher.group("tempoBalizamento"),
                matcher.group("tempoFinal"), 
                IT, 
                pontos);
	}
	
	public static QuedaNaAgua parseLine(String line) {
		Matcher matcher = createAlternativeLineMatcher(line);

		if (matcher.find()) {
			return matcherToQuedaNaAgua(matcher);
		}
		else {
            System.out.println("erro na linha abaixo");
            System.out.println(line);
            return null;
		}
	}
}
