package readPdf.result;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import pesquisa.BuscaResultadoClube;
import readPdf.IReadPdf;
import swim.Prova;
import swim.ProvaFinalizada;
import swim.enums.Categoria;
import swim.enums.Distancia;
import swim.enums.Estilo;
import swim.enums.Naipe;

public class ReadResult implements IReadPdf{

	private static boolean debug = false;
	
	//estadual de inverno mirim petiz 2022
	//obs: provas sem ninguem 33, 44, 65, 66, 
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/36045/01360450000000F000000010000000000001363.pdf";
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/37068/01370680000000F000000010000000000001387.pdf";
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/37071/01370710000000F000000010000000000001387.pdf";
	
	//estadual de verao mirim petiz - so tem uma prova
    //private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/36051/01360510000000F000000010000000000101363.pdf";

	//sulbra jr sr, etapa 1, 2023
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/1a3bb734b7fa2c283c0082f3b0814842.pdf";
	//etapa 3
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/ce8a7bfd57ecb5cc4a790fb5107e2293.pdf";
	//etapa 3 finais open
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/770dd2ccb4047e991a5ca7e0abbaa339.pdf";
	
	//estadual de inverno 2023
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/ef59ffd9279b955bc0fc30d1328e2308.pdf";
	//resultado absoluto
	//private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/50d09f98b09c606f3c81008c512fe693.pdf";
	
	//estadual de verao 2023
	private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/37116/01371160000000F000000010000000000001351.pdf";

    //brasileiro infantil verao 2023
    //eliminatorias
    //private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/37923/01379230000000E000000010000000000100976.pdf";
    //finais
    //private String pdfUrl = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/37923/01379230000000F000000010000000000101126.pdf";
	
    /**
     * Expressao regular que identifica uma prova
     */
    private static final String regexpProva = 
    		"(?<distancia>50|100|200|400|800|1500|"
    		+ "(REV\\.|REVEZAMENTO)?(\\s)?(4|8)(\\s)?X(\\s)?50|"
    		+ "(REV\\.)?(\\s)?4(\\s)?X(\\s)?100|"
    		+ "(REV\\.)?(\\s)?4(\\s)?X(\\s)?200)"
    		+ "\\s?"
            + "((METROS|M)\\s)?"
    		+ "(?<estilo>(\\s)?(BORBOLETA|COSTAS|PEITO|LIVRE|L|MEDLEY))\\s"
            + "(?<naipe>FEMININO|FEM|MASCULINO|MASC|MISTO)"
            //+ "(.)*"
            + "(\\s-\\s"
            + "(?<categoria>ABSOLUTO|PRE-MIRIM|MIRIM\\s1|MIRIM\\s2|MIRIM|PETIZ\\s1|PETIZ\\s2|PETIZ|"
	            + "INFANTIL\\s1|INFANTIL\\s2|INFANTIL|"
	            + "JUVENIL\\s1|JUVENIL\\s2|JUVENIL|"
	            + "JUNIOR\\s1|JUNIOR\\s2|JUNIOR|"
	            + "SENIOR|"
	            + "Juvenil a Sênior|))?"
            ;

    /**
     * cabecalhos do pdf, que devem excluidos
     */
    private HashSet<String> cabecalhos;
    /**
     * rodapes do pdf, tambem devem ser excluidos
     */
    private HashSet<String> rodape;

    /**
     * lista de Provas do Pdf, contem todas as informaçoes da competicao
     */
    private ArrayList<Prova> provas;
    /**
     * lista de Clubes presentes nas provas, usados para filtragem
     */
    private HashSet<String> clubes;
    /**
     * prova em que cada etapa inicia, obviamente a etapa 1 comeca na prova 1
     */
    //private ArrayList<Integer> inicioEtapas;
    private String nomeCompeticao;

    /**
     * Construtor
     */
    public ReadResult() {
        provas = new ArrayList<Prova>();
        cabecalhos = new HashSet<String>();
        rodape = new HashSet<String>();
        clubes = new HashSet<String>();
        //inicioEtapas = new ArrayList<>();
    }

    /**
     * obtem cabecalhos do pdf, utilizados para filtragem
     * @param s
     */
    private void obterCabecalhos(String s) {
        String[] p = s.split("\n");
        for (int i = 0; i < p.length - 1; i++) {	//ultima linha faz parte da primeira prova
            cabecalhos.add(p[i]);
        }
    }

    /**
     *
     * @param docSplit
     */
    private void obterRodape(String[] docSplit) {
        for (int i = 0; i < docSplit.length; i++) {
            if (docSplit[i].contains("Versão")) {
                String[] lines = docSplit[i].split("\n");
                for (String l : lines) {
                    if (l.contains("Versão") ||
                    		l.contains("Escaneie o qr code e cadastre-se") ||
                    		l.contains("CBDA Vida Atleta de Vantagens") ||
                    		l.contains("SGE - Maior plataforma de Gestão Esportiva do país") ||
                    		l.contains("Software de Apuração Oficial de Natação")) 
                    {
                        rodape.add(l);
                        //System.out.println(l);
                    }
                }
            }
        }
    }

    private String categoriaLinhaAcima(String doc, int index) {
    	int beginIndex = doc.indexOf("\n", index-15);
    	String cat = doc.substring(beginIndex+1, index-1);
    	return cat.replaceAll(" ", "_");
    }
    
    public void criaListaDeProvas(String doc) {
        Pattern p = Pattern.compile(regexpProva);
        Matcher matcher = p.matcher(doc.toUpperCase().replaceAll("Ú", "U").replaceAll("Ê", "E")); //Júnior, Sênior
        int i = 0;
        ProvaFinalizada prova;
        //System.out.println(doc.toUpperCase().replaceAll("Ú", "U").replaceAll("Ê", "E"));
        while (matcher.find()) {
            i++;
            if (debug) System.out.println(i + " " + matcher.group());
            
            prova = new ProvaFinalizada();
            prova.setNumero(i);
            prova.setDistancia(Distancia.valueOf("_" + matcher.group("distancia")
                .replaceAll(" X ","X").replaceAll("REVEZAMENTO ", "").replaceAll("REV.(\\s)?", "") ));
            prova.setEstilo(Estilo.valueOf(matcher.group("estilo").trim()));
            prova.setNaipe(Naipe.valueOf(matcher.group("naipe").trim()));
            String cat = matcher.group("categoria");
            if (cat == null) cat = categoriaLinhaAcima(doc, matcher.start());
            if (!cat.isEmpty()) {
                prova.setCategoria(Categoria.valueOf(cat.trim().toUpperCase().replaceAll(" ", "_").replaceAll(" X ","X").replaceAll("-","_")));
            }
            this.provas.add(prova);
        }
        if (debug) System.out.println("total "+ i);
    }

    private int firstNotNumberBeforeIndex(String source, int index) {
    	int i = index;
    	while (i > 0) {
    		char c = source.charAt(--i);
    		if (c != '0' && c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6' && c != '7' && c != '8' && c != '9')
    		{
    			return i;
    		}
    	}
    	return -1;
    }
    
    private int getNumProva(String raw) {
    	int indexEndProva = raw.indexOf("ª");
    	int indexBeginProva = firstNotNumberBeforeIndex(raw, indexEndProva-1);
    	
    	//se for um espaco em branco tenho que remover um char a mais, ex:
    	//pre mirim13 -> pre mirim, prova 13
    	//mirim 215 -> mirim 2, prova 15
    	if (raw.charAt(indexBeginProva) == ' ') indexBeginProva++;
    	
        return Integer.parseInt(raw.substring(indexBeginProva+1, indexEndProva).trim());
    }
    
    public void lerPdf() {
        System.setProperty("http.agent", "Chrome"); //sem isso da erro 403
        InputStream input;
        try {
            input = new URL(this.getPdfUrl()).openStream();

            PDDocument pddDocument = PDDocument.load(input);
            PDFTextStripper textStripper = new PDFTextStripper();
            String doc = textStripper.getText(pddDocument);
            pddDocument.close();

            nomeCompeticao = doc.substring(0, doc.indexOf("\n"));
            
            doc = doc.replaceAll("BORBOL\\.", "BORBOLETA");
            doc = doc.replaceAll("NADO LIVRE", "LIVRE");

            String[] splitPorProva = doc.split(ProvaDelimiter);

            //obtem cabecalhos e rodapes, exclui do documento, e separa por provas de novo
            this.obterCabecalhos(splitPorProva[0]);
            for (String c : cabecalhos) {
                doc = doc.replaceAll(c, "");
            }
            this.obterRodape(splitPorProva);
            for (String r : rodape) {
                doc = doc.replaceAll(r, "");
            }
            splitPorProva = doc.split(ProvaDelimiter);

            this.criaListaDeProvas(doc);

            for (int i = 0; i < splitPorProva.length-1; i++) {
            	int numProva;
            	try {
            		numProva = getNumProva(splitPorProva[i]);
            	} catch (IndexOutOfBoundsException e) {
            		numProva = i+1;
            	} catch (NumberFormatException e) {
                    //adicionei esse pra funcionar com bra inf 2023, mas nao encontrei o exato problema
                    numProva = provas.get(i-1).getNumero() + 1;
                }
            	if (debug) System.out.println(provas.size() + " " + i + " " + numProva +"________________________________");
                ((ProvaFinalizada) provas.get(i)).lerResultadoProva(splitPorProva[i+1]);
                provas.get(i).setNumero(numProva);
                //p.setDistancia(distancia);
                //p.setEstilo(estilo);
                clubes.addAll(provas.get(i).getClubes());
            }
            if (debug) System.out.println("TOTAL LIDO " + provas.size());
        } catch (IOException e) {
            System.err.println("Erro ao obter pdf");
            System.err.println(e.getMessage());
        }
    }

    public ArrayList<Prova> getProvas() {
        return this.provas;
    }

    public String getPdfUrl() {
        return pdfUrl;
    }

    public void setPdfUrl(String p) {
        pdfUrl = p;
    }

    public HashSet<String> getClubes() {
        return clubes;
    }
    
    public String getNomeCompeticao(){
        return nomeCompeticao;
    }

    /**
     * main para testar o programa sem a interface grafica
     * @param args 
     */
    public static void main(String args[]) {
        ReadResult leitorPdf = new ReadResult();
        if (args.length > 1) {
            leitorPdf.setPdfUrl(args[0]);
        }
        leitorPdf.lerPdf();

        System.out.println(leitorPdf.getClubes());
        
        BuscaResultadoClube b = new BuscaResultadoClube("CAIXEIROS", leitorPdf.nomeCompeticao, leitorPdf.provas);
        b.montaListadeAtletas();
        b.writeExcelFile("");
    }
}
