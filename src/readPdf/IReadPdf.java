package readPdf;

public interface IReadPdf {

	public final static String ProvaDelimiter = " PROVA|Prova N°";
	
	public abstract void lerPdf();
}
