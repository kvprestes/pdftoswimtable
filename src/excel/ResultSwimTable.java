package excel;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;

import swim.enums.Categoria;
import swim.enums.Distancia;
import swim.enums.Estilo;
import swim.enums.Naipe;

public class ResultSwimTable extends SwimTable{

	public ResultSwimTable() {
		init();
	}

	public void init() {
		wb = new HSSFWorkbook();
		// Workbook wb = new XSSFWorkbook();
		createHelper = wb.getCreationHelper();
		sheet = wb.createSheet("new sheet");

		sheet.setColumnWidth(0, 1500); // prova
		sheet.setColumnWidth(1, 1800); // genero
		sheet.setColumnWidth(2, 2700); // categoria

		sheet.setColumnWidth(3, 6500); // nome
		sheet.setColumnWidth(4, 2000); // tempo balizado
		sheet.setColumnWidth(5, 2200); // tempo oficial
		sheet.setColumnWidth(6, 1500); // colocacao
		sheet.setColumnWidth(7, 1800); // pontos
		sheet.setColumnWidth(8, 1500); // it
	}

	/**
	 * Escreve cabecalho do arquivo excel
	 */
	public void writeHeader(int rowNum, String nomeCompeticao) {
		Row row = sheet.createRow((short) rowNum);
		writeCell(row, 0, nomeCompeticao);
		row = sheet.createRow((short) rowNum + 1);
		int colIndex = 0;
		writeCell(row, colIndex++, "Prova");
		writeCell(row, colIndex++, "Gênero");
		writeCell(row, colIndex++, "Categoria");
		writeCell(row, colIndex++, "Nome");
		writeCell(row, colIndex++, "Tempo Balizado");
		writeCell(row, colIndex++, "Tempo obtido");
		writeCell(row, colIndex++, "Colocação");
		writeCell(row, colIndex++, "Pontos");
		writeCell(row, colIndex++, "IT");
	}

	/**
	 * Escreve uma linha na tabela excel
	 *
	 * @param rowNum
	 * @param prova
	 * @param categoria
	 * @param nome
	 */
	public int writeRow(int rowNum, 
			Distancia distancia, Categoria categoria, Naipe naipe, Estilo estilo, 	//prova 
			String nome, String tempo, int colocacao, String pontos, String it 		//atleta
			) {
		// Create a row and put some cells in it. Rows are 0 based.
		Row row = sheet.createRow((short) rowNum);
		String prova = distancia.toString().replace("_", "") + estilo.toString().charAt(0);

		//line format:
		//Prova - gênero - categoria - nome do atleta - tempo balizado - tempo obtido - colocação - pontos - IT
		writeCell(row, 0, prova);
		writeCell(row, 1, (naipe == null ? "" : naipe.toString()));
		writeCell(row, 2, (categoria == null ? "" : categoria.toString()));
		writeCell(row, 3, nome);
		writeCell(row, 4, ""); //tempo balizado
		writeCell(row, 5, tempo);
		writeCell(row, 6, Integer.toString(colocacao));
		writeCell(row, 7, pontos);
		writeCell(row, 8, it);

		return ++rowNum;
	}

	public void writeCell(Row row, int cellIndex, String cellValue) {
		// Create a cell and put a value in it.
		Cell cell = row.createCell(cellIndex);
		cell.setCellValue(cellValue);
	}

	public void writeToFile(String filename) throws IOException {
		// Write the output to a file
		FileOutputStream fileOut = new FileOutputStream(filename);

		sheet.setPrintGridlines(true);
		sheet.setFitToPage(true);
		PrintSetup ps = sheet.getPrintSetup();
		ps.setFitWidth((short) 1);
		ps.setFitHeight((short) 0);

		wb.write(fileOut);
		fileOut.close();
	}
}
