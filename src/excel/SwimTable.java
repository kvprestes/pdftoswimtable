package excel;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.PrintSetup;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;

import swim.enums.Distancia;
import swim.enums.Estilo;

public class SwimTable {

    protected Sheet sheet;
    protected CreationHelper createHelper;
    protected Workbook wb;

    private boolean showPreviousTime_;

    public SwimTable() {
        showPreviousTime_ = true;
        init();
    }

    public SwimTable(boolean showPreviousTime) {
        showPreviousTime_ = showPreviousTime;
        init();
    }

    public void init() {
		wb = new HSSFWorkbook();
		// Workbook wb = new XSSFWorkbook();
		createHelper = wb.getCreationHelper();
		sheet = wb.createSheet("new sheet");

		int col_index = 0;
		sheet.setColumnWidth(col_index++, 1500); // prova
		sheet.setColumnWidth(col_index++, 5600); // nome
		sheet.setColumnWidth(col_index++, 3000); // genero
		sheet.setColumnWidth(col_index++, 1200); // ano nascimento 
		sheet.setColumnWidth(col_index++, 2100); // serie.raia

		sheet.setColumnWidth(col_index++, 2000); // parcial 1
		sheet.setColumnWidth(col_index++, 2000); // parcial 2
		sheet.setColumnWidth(col_index++, 2000); // parcial 3
		sheet.setColumnWidth(col_index++, 2000); // parcial 4

		sheet.setColumnWidth(col_index++, 1500); // tempo total
		sheet.setColumnWidth(col_index++, 1500); // oficial
		sheet.setColumnWidth(col_index++, 1200); // colocacao
		sheet.setColumnWidth(col_index++, 1700); // pontos
	}

    /**
     * Escreve cabecalho do arquivo excel
     */
    public void writeHeader(int rowNum, String nomeCompeticao) {
        Row row = sheet.createRow((short) rowNum);
        writeCell(row, 0, nomeCompeticao);
        row = sheet.createRow((short) rowNum + 1);
        int colIndex = 0;
        writeCell(row, colIndex++, "Prova");
        writeCell(row, colIndex++, "Nome");
        writeCell(row, colIndex++, "série.raia");
        writeCell(row, colIndex++, "Genero");
        writeCell(row, colIndex++, "Ano");
        if (showPreviousTime_) {
            writeCell(row, colIndex++, "tempo");
        }
        writeCell(row, colIndex++, "parcial 1");
        writeCell(row, colIndex++, "parcial 2");
        writeCell(row, colIndex++, "parcial 3");
        writeCell(row, colIndex++, "parcial 4");
        writeCell(row, colIndex++, "total");
        writeCell(row, colIndex++, "oficial");
        writeCell(row, colIndex++, "col.");
        writeCell(row, colIndex++, "pontos");
    }

    /**
     * Escreve uma linha na tabela excel
     *
     * @param rowNum
     * @param prova
     * @param nome
     * @param serie_raia
     */
    public int writeRow(int rowNum, 
            Distancia distancia, 
            Estilo estilo, 
            String nome,
            String naipe,
            int ano_nascimento,
            String serie_raia, 
            String tempo) {
        // Create a row and put some cells in it. Rows are 0 based.
        Row row = sheet.createRow((short) rowNum);
        String prova = distancia.toString().replace("_", "") + estilo.toString().charAt(0);
        writeCell(row, 0, prova);
        writeCell(row, 1, nome);
        writeCell(row, 2, serie_raia);
        writeCell(row, 3, naipe);
        writeCell(row, 4, ((Integer)ano_nascimento).toString());
        if (showPreviousTime_) {
            writeCell(row, 5, tempo);
        }

        int nextRow = rowNum++;
        // algumas provas precisam de mais linhas
        switch (distancia) {
        case _800:
        case _400:
            row = sheet.createRow((short) rowNum);
            writeCell(row, 0, "");
            rowNum++;
            break;
        case _1500:
            row = sheet.createRow((short) rowNum);
            writeCell(row, 0, "");
            rowNum++;
            row = sheet.createRow((short) rowNum);
            writeCell(row, 0, "");
            rowNum++;
            row = sheet.createRow((short) rowNum);
            writeCell(row, 0, "");
            rowNum++;
            break;
        default:
            break;
        }
        return rowNum;
    }

    public void writeCell(Row row, int cellIndex, String cellValue) {
        // Create a cell and put a value in it.
        Cell cell = row.createCell(cellIndex);
        cell.setCellValue(cellValue);
    }

    public void setarQuebraDePagina(Integer quebra) {
        sheet.setRowBreak(quebra);
    }

    public void writeToFile(String filename) throws IOException {
        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(filename);

        // sheet.setFitToPage(true);

        // sheet.autoSizeColumn(0);
        // sheet.autoSizeColumn(1);
        // sheet.autoSizeColumn(2);
        // sheet.autoSizeColumn(3);
        // sheet.autoSizeColumn(4);
        // sheet.autoSizeColumn(5);
        // sheet.autoSizeColumn(6);
        // sheet.autoSizeColumn(7);
        // sheet.autoSizeColumn(8);
        // sheet.autoSizeColumn(9);
        // sheet.autoSizeColumn(10);

        // sheet.setAutobreaks(true);
        // sheet.setDisplayGridlines(true);
        sheet.setPrintGridlines(true);
        sheet.setFitToPage(true);
        PrintSetup ps = sheet.getPrintSetup();
        ps.setFitWidth((short) 1);
        ps.setFitHeight((short) 0);

        wb.write(fileOut);
        fileOut.close();
    }
}
