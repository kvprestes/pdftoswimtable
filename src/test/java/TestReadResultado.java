package test.java;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.junit.Test;

import readPdf.result.ReadResult;
import readPdf.result.ReadResultLine;
import swim.ProvaFinalizada;
import swim.QuedaNaAgua;
import swim.enums.Categoria;
import swim.enums.Distancia;
import swim.enums.Estilo;
import swim.enums.Naipe;

public class TestReadResultado {

	// estadual de inverno mirim petiz 2022
	private String t1 = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/36045/01360450000000F000000010000000000001363.pdf";
	// aqui a pontuacao = 18 16 15 14 13 12 11 10 9 7 6 5 4 3 2 1
	// sem o 8

	// abertura de temporada 2023
	private String t2 = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/37068/01370680000000F000000010000000000001387.pdf";

	// mirim petiz 2023
	private String t3 = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/37071/01370710000000F000000010000000000001387.pdf";
	// aqui a pontuacao = 18 16 15 14 13 12 11 10 9 8 7 6 5 4 3 2
	// com o 8, sem o 1

	// sulbra jr sr, etapa 1, 2023
	private String t4 = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/1a3bb734b7fa2c283c0082f3b0814842.pdf";

	@Test
	public void matchLine() {
        String line = "2 5 LAURA SOUZA GOMES  2010 GNU 02:43.54355471 32918,001";
        QuedaNaAgua qn = ReadResultLine.parseLine(line);
        assertEquals("LAURA SOUZA GOMES", qn.getNome());
        assertEquals(1, qn.getColocacao());
        assertEquals(2010, qn.getAnoNascimento());
        assertEquals("02:43.54", qn.getTempoFinal());
        assertEquals("355471", qn.getCod_cbda());
        assertEquals("18,00", qn.getPontos());
        assertEquals("GNU", qn.getClube());
        
        String lineEmpate = "2 4 AMANDA KELLER 2010 RECREIO 00:35.13 E370039 30514,503";
        QuedaNaAgua qnE = ReadResultLine.parseLine(lineEmpate);
        assertEquals("AMANDA KELLER", qnE.getNome());
        assertEquals(3, qnE.getColocacao());
        assertEquals(2010, qnE.getAnoNascimento());
        assertEquals("00:35.13", qnE.getTempoFinal());
        assertEquals("370039", qnE.getCod_cbda());
        assertEquals("14,50", qnE.getPontos());
        assertEquals("RECREIO", qnE.getClube());
        
        String line2 = "3 6 DAVI BARISON 2012 CAIXEIROS 00:58.56376067 673,0014";
        QuedaNaAgua qn2 = ReadResultLine.parseLine(line2);
        assertEquals("67", qn2.getIT());
        assertEquals("3,00", qn2.getPontos());
        assertEquals("CAIXEIROS", qn2.getClube());
        
        // problema: como separar o indice tecnico da pontuacao?
        String lineRev = "1 4 CAIXEIROS VIAJANTES  \"A\" 1800 CAIXEIROS 03:15.67280431 036,001";
        QuedaNaAgua qnRev = ReadResultLine.parseLine(lineRev);
        assertEquals("0", qnRev.getIT());
        assertEquals("36,00", qnRev.getPontos());
        assertEquals("CAIXEIROS", qnRev.getClube());
        
        String lineNoPoints = "2 7 ARTHUR KREMER 2012 RECREIO DA 03:41.30377871 902";
        QuedaNaAgua qnNP = ReadResultLine.parseLine(lineNoPoints);
        assertEquals("ARTHUR KREMER", qnNP.getNome());
        assertEquals("03:41.30", qnNP.getTempoFinal());
        assertEquals("90", qnNP.getIT());
        assertEquals("0,00", qnNP.getPontos());
        assertEquals("RECREIO DA", qnNP.getClube());
        
        String l = "1 2 THIAGO MOSSMANN 2014 CAIXEIROS 02:22.94393128 58,0010";
        QuedaNaAgua qnL = ReadResultLine.parseLine(l);
        assertEquals("THIAGO MOSSMANN", qnL.getNome());
        assertEquals("02:22.94", qnL.getTempoFinal());
        assertEquals("5", qnL.getIT());
        assertEquals("8,00", qnL.getPontos());
        assertEquals("CAIXEIROS", qnL.getClube());
	}

	@Test
	public void matchLineProva() {
        ReadResult leitorPdf = new ReadResult();
        String line = "400 METROS LIVRE FEMININO - PETIZ 11 ª PROVA 16/04/2023\n"
                + "Col. S R Nome Nasc. EntidadePatrocínio Tempo \n" + "Obtido\n" + "Reg. ITPts.C.\n"
                + "2 2 MARIA FERNANDA CERIOTTI 2012 RECREIO DA 06:01.59373477 27018,001º\n"
                + "2 3 ANITA APPEL BARBOSA 2012 GNU 06:27.75371081 21916,002º\n"
                + "2 1 CRISTAL ANGONESE 2012 RECREIO DA 06:30.00374093 21515,003º\n"
                + "1 1 MELISSA SCHUETZ RAMOS 2012 CAIXEIROS 06:35.86376070 20614,004º\n"
                + "2 4 GABRIELA BAISCH 2012 GNU 06:44.05382058 19413,005º\n"
                + "1 6 RAFAELA MARIAH 2012 CAIXEIROS 06:52.55387988 18212,006º\n"
                + "2 5 MANUELA ROSSI 2012 RECREIO DA 06:56.87373501 17611,007º\n"
                + "2 6 LAURA GIACOMIN 2012 RECREIO DA 07:05.42386487 16610,008º\n"
                + "1 5 ANA CAROLINA DAMIAN 2012 RECREIO DA 07:30.67392504 1399,009º\n"
                + "1 2 YASMIN SANTOS GUERRA 2012 RECREIO DA 07:50.61392502 1228,0010º\n"
                + "1 4 ANA LUIZA GHIOTTI 2012 RECREIO DA 07:57.14387661 1177,0011º\n"
                + "1 3 AMANDA RAVIZZONI 2012 RECREIO DA 387658N/C\n"
                + "400 METROS LIVRE FEMININO - PETIZ 22 ª PROVA 16/04/2023\n"
                + "Col. S R Nome Nasc. EntidadePatrocínio Tempo";
        leitorPdf.criaListaDeProvas(line);
        assertEquals(2, leitorPdf.getProvas().size());
        assertEquals(Distancia._400, leitorPdf.getProvas().get(0).getDistancia());
        assertEquals(Estilo.LIVRE, leitorPdf.getProvas().get(0).getEstilo());
        assertEquals(Naipe.FEMININO, leitorPdf.getProvas().get(0).getNaipe());
        assertEquals(Categoria.PETIZ_1, leitorPdf.getProvas().get(0).getCategoria());
	}

	/**
	 * Testa a leitura da prova no formato em que a categoria fica na linha acima
	 * por exemplo no sulbra jr/sr de 2023
	 */
	@Test
	public void matchLineProva2() {
        ReadResult leitorPdf = new ReadResult();
        String line2 = "Etapa: 1 (4/5/2023)\n" + "\n" + "\n" + "NATAÇÃO\n" + "\n" + "\n" + "\n" + "1Prova N°\n"
                + "JUNIOR 1\n" + "400 METROS LIVRE FEMININO\n"
                + "RS MARIA PAULA HEITMANN UNISANTA RECIFE-PE04:03.45 10/08/2022ABSOLUTO\n"
                + "RB MARIA PAULA HEITMANN UNISANTA RECIFE-PE04:03.45 10/08/2022ABSOLUTO\n"
                + "RC RAFAELA TREVISAN RAURICH CURITIBANO CLUBE DOZE DE AGOSTO04:11.87 25/05/2017JUNIOR 1\n"
                + "RC RAFAELA TREVISAN RAURICH CURITIBANO CLUBE DOZE DE AGOSTO04:13.76 17/05/2018JUNIOR 2\n"
                + "RC GIULIA SALESI CHICON CURITIBANO FLORIANOPOLIS-SC04:18.98 04/05/2023 SENIOR\n"
                + "Col. S R Nome Nasc. EntidadePatrocínio Tempo \n" + "Obtido\n" + "Reg. ITPts.C.\n"
                + "2 6 ALESSANDRA HAAS REN 2006 GNU 04:26.36343997 67718,001°\n"
                + "2 1 ANA CLARA SANTIN RONCHI 2006 SANTA MÔNICA 04:35.50317049 61216,002°\n"
                + "1 6 ANALYCE NUNES PORTO LUZ 2006 SANTA MÔNICA 04:49.18369322 52915,003°\n"
                + "1 4 MARIANA CRUZZOLINI ALVES 2006 LIRA 04:56.68366375 49014,004°\n" + "\n" + "\n" + "\n"
                + "Etapa: 1 (4/5/2023)\n" + "\n" + "\n" + "NATAÇÃO\n" + "\n" + "\n" + "\n" + "1Prova N°\n"
                + "JUNIOR 2\n" + "400 METROS LIVRE FEMININO\n"
                + "RS MARIA PAULA HEITMANN UNISANTA RECIFE-PE04:03.45 10/08/2022ABSOLUTO\n"
                + "RB MARIA PAULA HEITMANN UNISANTA RECIFE-PE04:03.45 10/08/2022ABSOLUTO";
        leitorPdf.criaListaDeProvas(line2);
        assertEquals(2, leitorPdf.getProvas().size());
        assertEquals(Distancia._400, leitorPdf.getProvas().get(1).getDistancia());
        assertEquals(Estilo.LIVRE, leitorPdf.getProvas().get(1).getEstilo());
        assertEquals(Naipe.FEMININO, leitorPdf.getProvas().get(1).getNaipe());
        assertEquals(Categoria.JUNIOR_2, leitorPdf.getProvas().get(1).getCategoria());
	}

	@Test
	public void revAmbiguousLine() {
        String l = "1 4 CAIXEIROS VIAJANTES  \"A\" 1800 CAIXEIROS 05:08.79280431 2532,002";
        QuedaNaAgua qnL = ReadResultLine.parseLine(l);
        assertEquals("CAIXEIROS VIAJANTES  \"A\"", qnL.getNome());
        assertEquals("05:08.79", qnL.getTempoFinal());
        assertEquals("25", qnL.getIT());
        assertEquals("32,00", qnL.getPontos());
	}

	@Test
	public void testRead1() {
        ReadResult leitorPdf = new ReadResult();
        leitorPdf.setPdfUrl(t1);
        leitorPdf.lerPdf();
        // 70 provas, 4 vazias->33, 44, 65, 66
        assertEquals(70 - 4, leitorPdf.getProvas().size());
	}

	@Test
	public void testRead2() {
        ReadResult leitorPdf = new ReadResult();
        leitorPdf.setPdfUrl(t2);
        leitorPdf.lerPdf();
        assertEquals(34, leitorPdf.getProvas().size());
	}

	@Test
	public void testRead3() {
        ReadResult leitorPdf = new ReadResult();
        leitorPdf.setPdfUrl(t3);
        leitorPdf.lerPdf();
        assertEquals(68, leitorPdf.getProvas().size());
        // verifica revezamento
        ProvaFinalizada prova = ((ProvaFinalizada) leitorPdf.getProvas().get(19));
        assertEquals(Categoria.MIRIM_2, prova.getCategoria());
        ArrayList<QuedaNaAgua> resultRev = prova.getResultadoFinal();
        assertEquals(4, resultRev.size());
        assertEquals("03:23.99", resultRev.get(0).getTempoFinal());
        assertEquals("GNU \"A\"", resultRev.get(0).getNome());
        assertEquals("03:32.97", resultRev.get(1).getTempoFinal());
        assertEquals("CAIXEIROS VIAJANTES  \"A\"", resultRev.get(1).getNome());
        assertEquals("03:49.25", resultRev.get(2).getTempoFinal());
        assertEquals("GNU \"B\"", resultRev.get(2).getNome());
        assertEquals("03:53.38", resultRev.get(3).getTempoFinal());
        assertEquals("RECREIO DA JUVENTUDE \"A\"", resultRev.get(3).getNome());
	}

	@Test
	public void testRead4() {
        ReadResult leitorPdf = new ReadResult();
        leitorPdf.setPdfUrl(t4);
        leitorPdf.lerPdf();
        assertEquals(30, leitorPdf.getProvas().size());
        ArrayList<QuedaNaAgua> result = ((ProvaFinalizada) leitorPdf.getProvas().get(23)).getResultadoFinal();
        assertEquals(5, result.size());
        assertEquals("02:15.04", result.get(3).getTempoFinal());
        assertEquals("KASSIUS PRESTES", result.get(3).getNome());
	}
	
	@Test
    public void testReadRev8x50() {
        ReadResult leitorPdf = new ReadResult();
        String line = "1 5 FERNANDO RAMOS  DE 2008 CAIXEIROS366844 0,00N/C\n"
                + "Revezamento 8x50 Livre Misto - Até 116 anos33ª PROVA 15/07/2023\n"
                + "Col. S R Nome Nasc. EntidadePatrocínio Tempo \n"
                + "Obtido\n"
                + "Reg. ITPts.C.\n"
                + "1 3 GNU \"A\" 1800 GNU 03:34.89220014A 036,001º\n"
                + "1 4 GNU \"B\" 1800 GNU 03:35.18220014B 032,002º\n"
                + "1 6 RECREIO DA JUVENTUDE \"A\" 1800 RECREIO DA 03:47.19220072A 030,003º\n"
                + "1 2 CB - RS  \"A\" 1800 C. BRILHANTE 04:03.40220005A 028,004º\n"
                + "1 5 GNU \"A\" 1800 GNU389805 0,00N/C\n"
                + "\n"
                + "Revezamento 8x50 Livre Misto - ABSOLUTO34ª PROVA 15/07/2023\n"
                + "Col. S R Nome Nasc. EntidadePatrocínio Tempo \n"
                + "Obtido\n"
                + "Reg. ITPts.C.\n"
                + "1 4 GNU \"A\" 1800 GNU 03:25.92220014A 036,001º\n"
                + "1 5 RECREIO DA JUVENTUDE \"A\" 1800 RECREIO DA 03:36.58220072A 032,002º\n"
                + "1 3 CAIXEIROS VIAJANTES  \"A\" 1800 CAIXEIROS 03:47.46280431 030,003º";
        leitorPdf.criaListaDeProvas(line);
        assertEquals(2, leitorPdf.getProvas().size());
        //assertEquals(Distancia._400, leitorPdf.getProvas().get(1).getDistancia());
        assertEquals(Estilo.LIVRE, leitorPdf.getProvas().get(1).getEstilo());
        assertEquals(Categoria.ABSOLUTO, leitorPdf.getProvas().get(1).getCategoria());
    }
	
	@Test
    public void testRead_UmaProvaPorPagina_PrimeiraLinhaComCabecalho() {
        String line = ""
                + "INFANTIL 1 "
                + "200 METROS BORBOLETA FEMININO "
                + "Col. S R Nome Nasc. EntidadePatrocínio Tempo "
                + "Obtido "
                + "Reg. ITPts.C. "
                + "2 1 LARA MELANY MENDOZA 2010 FENABA 02:32.55V404054 50918,001";
        QuedaNaAgua qnL = ReadResultLine.parseLine(line);
        assertEquals("LARA MELANY MENDOZA", qnL.getNome());
        assertEquals("02:32.55", qnL.getTempoFinal());
        assertEquals("509", qnL.getIT());
        assertEquals("18,00", qnL.getPontos());
        assertEquals(1, qnL.getColocacao());
	}

    @Test
    public void testRead_BrInfantil() {
        String line = "RT: 0\n" +
                "50M: 00:36.47 (00.00) 100M: 01:18.63 (42.16) 150M: 02:01.69 (43.60) 200M: 02:43.09 (41.40)\n" +
                "1 2 JULIA SCHMITT TOMAZI 2009 RECREIO DA JUVENTUDE02:43.37346939 4140,0013";
        QuedaNaAgua qnL = ReadResultLine.parseLine(line);
        assertEquals("JULIA SCHMITT TOMAZI", qnL.getNome());
        assertEquals("RECREIO DA JUVENTUDE", qnL.getClube());
        assertEquals("02:43.37", qnL.getTempoFinal());
    }

    @Test
    public void testReadRead_prova_clube_colocacao_separator() {
        ReadResult leitorPdf = new ReadResult();
        String line = null;
        try {
            line = new String(Files.readAllBytes(Paths.get("src/test/java/prova_ct1julho.txt")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        leitorPdf.criaListaDeProvas(line);
        assertEquals(1, leitorPdf.getProvas().size());
        assertEquals(Estilo.COSTAS, leitorPdf.getProvas().get(0).getEstilo());
        assertEquals(Categoria.INFANTIL_2, leitorPdf.getProvas().get(0).getCategoria());

        ProvaFinalizada p = (ProvaFinalizada)leitorPdf.getProvas().get(0);
        p.lerResultadoProva(line);
        assertEquals(43, p.getResultadoFinal().size());
    }

}
