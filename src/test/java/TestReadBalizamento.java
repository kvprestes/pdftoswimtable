package test.java;

import static org.junit.Assert.*;

import org.junit.Test;

import pesquisa.BuscaPorClube;
import readPdf.balizamento.ReadPdf;

public class TestReadBalizamento {

    private String t1 = "https://sge-aquaticos.bigmidia.com/_uploads/piscina/36045/10360450000000F000000000000000000001351.pdf";
    private String t2 = "https://sge-aquaticos.bigmidia.com/_uploads/boletim/o_1ftq4htl31riglbd1mvr1qdh3g2b.pdf";
    //Acho que eh o estadual de verao de 2021, esse link saiu do ar e nao achei na fgda
//    private String t3 = "https://sistema.cbdaweb.org.br/cbdaweb/_uploads/piscina/34317/103431700000000000000000000000000001128.pdf?x=1573828871";
    private String t4 = "http://www.fgda.com.br/site/wp-content/uploads/2013/04/BALIZAMENTO-TORNEIO-SUL-BRASILEIRO-DE-CLUBES-MIRIM-E-PETIZ-DE-NATA%C3%87%C3%83O-.pdf";
    private String t5 = "http://www.fgda.com.br/site/wp-content/uploads/2013/04/BALIZAMENTO-CAMPEONATO-ESTADUAL-DE-NATA%C3%87%C3%83O-DE-VER%C3%83O-MIRIM-A-SENIOR-2016.pdf";
    private String t6 = "http://www.fgda.com.br/site/wp-content/uploads/2013/04/BALIZAMENTO-TORNEIO-DE-ABERTURA-DE-TEMPORADA-2017.pdf";
    private String t7 = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/fbfd3f807c7c238a907c4d3b1efc5530.pdf";
    // outro formato de arquivo, nao funciona ainda
    private String t8 = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/356bc86a9e26f86264a7d1ed6be81df0.pdf";
    //estadual de inverno 2023
    private String t9 = "https://sge-aquaticos.bigmidia.com/_uploads/relatorios/89dc2de24291020c7bc31b3cdf28f473.pdf";

    private String clube = "CAIXEIROS";

    @Test
    public void testRead1() {
        ReadPdf leitorPdf = new ReadPdf();
        leitorPdf.setPdfUrl(t1);
        leitorPdf.lerPdf();
        // faltam 33, 44, 65, 66
        assertEquals(70 - 4, leitorPdf.getProvas().size());
        assertEquals("CAMPEONATO ESTADUAL MIRIM/PETIZ DE INVERNO", leitorPdf.getNomeCompeticao());
    }

    @Test
    public void testRead2() {
        ReadPdf leitorPdf = new ReadPdf();
        leitorPdf.setPdfUrl(t2);
        leitorPdf.lerPdf();
        assertEquals(34, leitorPdf.getProvas().size());
        assertEquals("TORNEIO ABERTURA DE TEMPORADA", leitorPdf.getNomeCompeticao());
    }

//    @Test
//    public void testRead3() {
//        ReadPdf leitorPdf = new ReadPdf();
//        leitorPdf.setPdfUrl(t3);
//        leitorPdf.lerPdf();
//        assertEquals(40, leitorPdf.getProvas().size());
//        assertEquals("CAMPEONATO DE VERÃO FEDERADOS JUVENIL/", leitorPdf.getNomeCompeticao());
//    }

    @Test
    public void testRead7() {
        ReadPdf leitorPdf = new ReadPdf();
        leitorPdf.setPdfUrl(t7);
        leitorPdf.lerPdf();
        // faltam 14, 19, 37, 46
        assertEquals(50 - 4, leitorPdf.getProvas().size());
        assertEquals("TORNEIO MIRIM/PETIZ 1º SEMESTRE", leitorPdf.getNomeCompeticao());
    }
    
    @Test
    public void testRead9() {
        ReadPdf leitorPdf = new ReadPdf();
        leitorPdf.setPdfUrl(t9);
        leitorPdf.lerPdf();
        assertEquals(54, leitorPdf.getProvas().size());
        assertEquals("CAMPEONATO ESTADUAL INFANTIL À SÊNIOR DE", leitorPdf.getNomeCompeticao());
    }

}
