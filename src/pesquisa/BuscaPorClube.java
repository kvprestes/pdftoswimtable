package pesquisa;

import java.io.IOException;
import java.util.ArrayList;

import excel.SwimTable;
import swim.NadadorBalizado;
import swim.Prova;
import swim.Serie;

public class BuscaPorClube {

	protected String clube;

    protected String competicao;
    protected ArrayList<Prova> provas;
    private ArrayList<Integer> inicioEtapas;

    private SwimTable st;
    
    protected boolean mostrarNomeAbreviado = false;

    public BuscaPorClube(String cl, String comp, ArrayList<Prova> p) {
    	clube = cl;
        provas = p;
        competicao = comp;
        inicioEtapas = new ArrayList<Integer>();
    }

    public void montaListadeAtletas( boolean showTime ) {
        st = new SwimTable(showTime);
        st.writeHeader(0, competicao);

        int etapaAtual = 1;
        int row = 2;
        for (Prova p : provas) {
            //System.out.println(p.getDistancia() + " " + p.getEstilo());
            ArrayList<Serie> series = p.getSeries();
            if (etapaAtual - 1 < inicioEtapas.size()) {
                if (p.getNumero() > inicioEtapas.get(etapaAtual - 1)) {
                    //nao precisa mais de quebra de pagina
                    //st.setarQuebraDePagina(row-1);
                    //st.writeHeader(row, competicao);
                    //row+=2;
                    etapaAtual++;
                }
            }
            for (int s = 0; s < series.size(); s++) {
                NadadorBalizado[] nadadores = series.get(s).getNadadores();
                for (NadadorBalizado n : nadadores) {
                    if (n != null) {
                        //System.out.println(n.getClube());
                        //System.out.println(n.getPrimeiroNome());
                        if (n.getClube().equals(clube)) {
                            //System.out.println(n.getClube());
                            //System.out.println(p.getNumero()+"."+(s+1) + " " + n.getPrimeiroNome());

                            //write row retorna a proxima linha a ser escrita
                            String nome = mostrarNomeAbreviado ?
                            		n.getPrimeiroNome() + " " + n.getUltimoNome()
                            		: n.getNome();
                            row = st.writeRow(row, 
                                    p.getDistancia(), 
                                    p.getEstilo(), 
                                    nome,
                                    p.getNaipe().toString(),
                                    n.getAnoNascimento(),
                                    ((Integer) (s + 1)).toString() + "." + n.getRaia(), 
                                    n.getTempo());
                        }
                    }
                }
            }
        }
    }

    public void setClube(String c) {
        this.clube = c;
    }

    public void setInicioEtapas(ArrayList<Integer> i) {
        inicioEtapas = i;
    }

    public void writeExcelFile(String filename) {
        try {
            st.writeToFile(clube.replace("/", "_") + ".xls");
        } catch (IOException e) {
            System.err.println("Erro ao escrever arquivo excel.");
        }
    }
}
