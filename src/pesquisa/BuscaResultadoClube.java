package pesquisa;

import java.io.IOException;
import java.util.ArrayList;

import excel.ResultSwimTable;
import swim.Prova;
import swim.ProvaFinalizada;
import swim.QuedaNaAgua;

public class BuscaResultadoClube extends BuscaPorClube{

    private ResultSwimTable rst;
    
    public BuscaResultadoClube(String cl, String comp, ArrayList<Prova> p) {
    	super(cl, comp, p);
    }

    public void montaListadeAtletas() {
    	rst = new ResultSwimTable();
    	rst.writeHeader(0, competicao);

        int row = 2;
        for (Prova p : provas) {
            for (QuedaNaAgua q : ((ProvaFinalizada)p).getResultadoFinal()) {
                //System.out.println(n.getClube());
                if (q.getClube().equals(clube)) {

                    //write row retorna a proxima linha a ser escrita
                    String nome = mostrarNomeAbreviado ? 
								q.getNomeAbreviado() : 
								q.getNome();
                    row = rst.writeRow(row, 
                    		p.getDistancia(), 
                    		p.getCategoria(),
                    		p.getNaipe(),
                    		p.getEstilo(), 
                    		nome,
                            q.getTempoFinal(), 
                            q.getColocacao(), 
                            q.getPontos(), 
                            q.getIT()
                            );
                }
            }
        }
    }

    public void writeExcelFile(String filename) {
        try {
            String time = java.time.LocalTime.now().toString();
            time = time.substring(time.lastIndexOf(".")+1);
            rst.writeToFile(clube.replace("/", "_")+ "_" + time + ".xls");
        } catch (IOException e) {
            System.err.println("Erro ao escrever arquivo excel.");
        }
    }
}
